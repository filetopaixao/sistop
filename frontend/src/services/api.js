import axios from 'axios'

const api = axios.create({
    baseURL: 'http://sistop.iacuft.org.br/'
})

export default api