import React, {useState, useMemo, useEffect } from 'react'
import api from '../../../services/api'
var createObjectURL = require('create-object-url');

const FormMunicipiosEdit = (props) => {
    var fileObj = [];
    var fileArray = [];
    const idUser = localStorage.getItem('userId')

    const [nome, setNome ] = useState('')
    const [image, setImage ] = useState('')
    const [toponimia, setToponimia ] = useState('')
    const [file, setFile] = useState([])

    useEffect(()=>{
        setNome(props.nome)
        setImage(props.image)
        setToponimia(props.toponimia)
    },[props.image, props.nome, props.toponimia])

    const preview = useMemo(() => {
        if(image != null){
            return typeof image != 'string' ? createObjectURL(image) : image;
        }
      }, [image])

    const uploadFiles = (e) => {
        e.preventDefault()
        console.log(file)
    }

    const handleNews = async (e) => {
        e.preventDefault()

        if(typeof image == 'string' ){
            var data = {
                nome,
                image: image,
                toponimia
            }
        }else{
            var data = new FormData();

            data.append('image', image);
            data.append('nome', nome);
            data.append('toponimia', toponimia);
        }

        try{
            await api.put('/municipios/' + props.id, data)
            
            alert('Município atualizada com sucesso!')
        }catch(err){
            alert('Erro ao atualizar a Município, tente novamente.')
        }
    }

    const deleteData = async (e) => {
        e.preventDefault()
        const r = window.confirm("Do you really want to delete it?");
        if(r == true){
            try{
                await api.delete('/munipios/' + props.id)
                
                alert('Município deletada com sucesso!')
            }catch(err){
                alert('Erro ao deletar a Município, tente novamente.')
            }
            //this.props.history.push('/admin')
        }
        //props.id
        
        
    }



    return(
        <form onSubmit={handleNews}>
            <div className="form-group">
                <label htmlFor="formGroupExampleInput2">Nome</label>
                <input type="text" className="form-control" placeholder="Título" value={nome} onChange={ e => setNome(e.target.value)}/>
            </div>

            <div className="form-group">
                <div><label htmlFor="formGroupExampleInput2">Imagem Destaque</label></div>
                <label 
                    id="thumbnail" 
                    style={{ backgroundImage: `url(${preview})`, width: '100%', height: '200px', backgroundSize: 'contain', backgroundRepeat: 'no-repeat' }}
                    className={image ? 'has-thumbnail' : ''}
                >
                    
                </label><input type="file" onChange={event => setImage(event.target.files[0])} name="gallery"/>
            </div>

            <div className="form-group">
                <label htmlFor="formGroupExampleInput2">Conteúdo</label>
                <textarea class="form-control" placeholder="Toponímia" rows="3" value={toponimia} onChange={ e => setToponimia(e.target.value)}></textarea>
            </div>
                
            
            <button type="submit" className="btn btn-primary">Atualizar</button>
            <button onClick={deleteData} className="btn btn-danger">Deletar</button>
        </form>
    )
}

export default FormMunicipiosEdit