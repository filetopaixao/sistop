import React from 'react'
import { GoogleMap, withScriptjs,
  withGoogleMap} from "react-google-maps"

  const exampleMapStyles = [
    {
        "featureType": "all",
        "elementType": "all",
        "stylers": [
            {
                "hue": "#008eff"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "saturation": "0"
            },
            {
                "lightness": "0"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            },
            {
                "saturation": "-60"
            },
            {
                "lightness": "-20"
            }
        ]
    }
];

const MyMapComponent = withScriptjs(
  withGoogleMap(props => (
    <GoogleMap
        defaultOptions={{
          styles: exampleMapStyles,
      }}
      defaultZoom={6}
      defaultCenter={{ lat: -10.1835604, lng: -48.3337793 }}
      onClick={(e) => props.onMapClick(e)}
    />
  ))
);
export default MyMapComponent;