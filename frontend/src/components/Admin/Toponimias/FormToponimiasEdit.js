import React, {useState, useMemo } from 'react'
import api from '../../../services/api'
import Map from './Map';
var createObjectURL = require('create-object-url');

const FormNoticiasEdit = (props) => {
    var fileObj = [];
    var fileArray = [];
    const idUser = localStorage.getItem('userId')

    const [nome, setNome ] = useState(props.nome)
    const [long, setLong ] = useState(props.long)
    const [lat, setLat ] = useState(props.lat)
    const [categoria, setCategoria ] = useState(props.categoria)
    const [image, setImage ] = useState(props.image)
    const [toponimia, setToponimia ] = useState(props.toponimia)
    const [file, setFile] = useState([])

    const preview = useMemo(() => {
        return typeof image != 'string' ? createObjectURL(image) : image;
      }, [image])

      const uploadMultipleFiles = (e) => {
        fileObj.push(e.target.files)
        for (let i = 0; i < fileObj[0].length; i++) {
            fileArray.push(URL.createObjectURL(fileObj[0][i]))
        }
        setFile( fileArray )
    }

    const uploadFiles = (e) => {
        e.preventDefault()
        console.log(file)
    }

    const handleNews = async (e) => {
        e.preventDefault()

        if(typeof image == 'string' ){
            var data = {
                nome,
                long,
                lat,
                categoria,
                image: image,
                toponimia
            }
        }else{
            var data = new FormData();

            data.append('image', image);
            data.append('nome', nome);
            data.append('long', long);
            data.append('lat', lat);
            data.append('categoria', categoria);
            data.append('toponimia', toponimia);
        }

        try{
            await api.put('/toponimias/' + props.id, data)
            
            alert('Toponímia atualizada com sucesso!')
        }catch(err){
            alert('Erro ao atualizar a Toponímia, tente novamente.')
        }
    }

    const deleteData = async (e) => {
        e.preventDefault()
        const r = window.confirm("Do you really want to delete it?");
        if(r == true){
            try{
                await api.delete('/toponimias/' + props.id)
                
                alert('Toponímia deletada com sucesso!')
            }catch(err){
                alert('Erro ao deletar a Toponímia, tente novamente.')
            }
            //this.props.history.push('/admin')
        }
        //props.id
        
        
    }



    return(
        <form onSubmit={handleNews}>
            <div className="form-group">
                <label htmlFor="formGroupExampleInput2">Nome</label>
                <input type="text" className="form-control" placeholder="Título" value={nome} onChange={ e => setNome(e.target.value)}/>
            </div>
            <div style={{width:'100%' , height: '300px'}}>
                <Map
                onMapClick={(e) => {
                    setLong(e.latLng.lat())     
                    setLat(e.latLng.lng())     
                    }
                }

                googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyAqBCX43JNpPBYs0AsNxB0bkipp--PqEK8"
                loadingElement={<div style={{ height: `100%` }} />}
                containerElement={<div style={{ height: `95vh` }} />}
                mapElement={<div style={{ height: `300px` }} />}
                lat={-34.397}
                lng={150.644}

            />
            </div>
            <div className="form-group">
                <label htmlFor="formGroupExampleInput2">Longitude</label>
                <input type="text" className="form-control" id="formGroupExampleInput2" placeholder="Longitude" value={long} onChange={ e => setLong(e.target.value)}/>
            </div>
            <div className="form-group">
                <label htmlFor="formGroupExampleInput2">Latitude</label>
                <input type="text" className="form-control" id="formGroupExampleInput2" placeholder="Latitude" value={lat} onChange={ e => setLat(e.target.value)} />
            </div>
            <div className="form-group">
                <label htmlFor="formGroupExampleInput2">Categoria</label>
                <input type="text" className="form-control" id="formGroupExampleInput2" placeholder="Categoria"  value={categoria} onChange={ e => setCategoria(e.target.value)}/>
            </div>

            <div className="form-group">
                <div><label htmlFor="formGroupExampleInput2">Imagem Destaque</label></div>
                <label 
                    id="thumbnail" 
                    style={{ backgroundImage: `url(${preview})`, width: '100%', height: '200px', backgroundSize: 'contain', backgroundRepeat: 'no-repeat' }}
                    className={image ? 'has-thumbnail' : ''}
                >
                    
                </label><input type="file" onChange={event => setImage(event.target.files[0])} name="gallery"/>
            </div>

            <div className="form-group">
                <label htmlFor="formGroupExampleInput2">Conteúdo</label>
                <textarea class="form-control" placeholder="Conteúdo da Notícia" rows="3" value={toponimia} onChange={ e => setToponimia(e.target.value)}></textarea>
            </div>
                
            
            <button type="submit" className="btn btn-primary">Atualizar</button>
            <button onClick={deleteData} className="btn btn-danger">Deletar</button>
        </form>
    )
}

export default FormNoticiasEdit