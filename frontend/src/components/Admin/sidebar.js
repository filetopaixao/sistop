import React from 'react'
import SideNav, {  NavItem, NavIcon, NavText } from '@trendmicro/react-sidenav';
import '@trendmicro/react-sidenav/dist/react-sidenav.css';
import styled from 'styled-components';
import TableList from '../../pages/Admin/Pages/TableList'
import api from '../../services/api'
import { FaHome, FaNewspaper, FaSignOutAlt , FaLightbulb, FaUserAlt, FaRegHandshake, FaUsers, FaProjectDiagram, FaRegMap, FaMapMarkerAlt } from "react-icons/fa";


const Main = styled.main`
    position: relative;
    overflow: hidden;
    transition: all .15s;
    padding: 0 20px;
    margin-left: ${props => (props.expanded ? 240 : 64)}px;
`;

export default class Sidebar extends React.Component{
    state = {
        selected: localStorage.getItem('selected'),
        expanded: false,
        data: {},
        title: ''
    };

    handleLogout = () => {
        const r = window.confirm("Do you really want to Sign Out?");
        if(r == true){
            localStorage.clear()
            this.props.history.push('/admin')
        }else{

        }
    }

    async componentDidMount(){
        if(this.state.selected == 'noticias'){
            this.setState({data:{} , title: 'Notícias'})
             await api.get('/noticias')
             .then(async res => {
                 await this.setState({data: res.data['noticias']})
             })
        }else if(this.state.selected == 'producao'){
             this.setState({data:{}, title: 'Produção Intelectual'})
             await api.get('/producoes')
             .then(async res => {
                 await this.setState({data: res.data['producoes']})
             })
        }else if(this.state.selected == 'projetos'){
            this.setState({data:{}, title: 'Projetos Vinculados'})
            await api.get('/projetos')
            .then(async res => {
                await this.setState({data: res.data['projetos']})
            })
       }else if(this.state.selected == 'user'){
            this.setState({data:{}, title: 'Usuários'})
            await api.get('/usuarios')
            .then(async res => {
                await this.setState({data: res.data['usuarios']})
            })
       }else if(this.state.selected == 'equipe'){
        this.setState({data:{}, title: 'Equipe'})
        await api.get('/equipe')
        .then(async res => {
            await this.setState({data: res.data['equipe']})
        })
   }else if(this.state.selected == 'parceiros'){
            this.setState({data:{}, title: 'Parceiros e Fomento'})
            await api.get('/parceiros')
            .then(async res => {
                await this.setState({data: res.data['parceiros']})
            })
       }else if(this.state.selected == 'municipios'){
            this.setState({data:{}, title: 'Municípios'})
            await api.get('/municipios')
            .then(async res => {
                await this.setState({data: res.data['municipios']})
            })
       }else if(this.state.selected == 'toponimias'){
            this.setState({data:{}, title: 'Toponímias'})
            await api.get('/toponimias')
            .then(async res => {
                await this.setState({data: res.data['toponimias']})
            })
       }
    }

    onSelect =  async (selected) => {
        if(selected == 'logout'){
            localStorage.setItem('selected', 'home')
            await this.setState({ selected: 'home' });
        }else{
            localStorage.setItem('selected', selected)
            await this.setState({ selected: selected });
        }

       if(selected == 'noticias'){
           this.setState({data:{} , title: 'Notícias'})
            await api.get('/noticias')
            .then(async res => {
                await this.setState({data: res.data['noticias']})
            })
       }else if(selected == 'producao'){
            this.setState({data:{}, title: 'Produção Intelectual'})
            await api.get('/producoes')
            .then(async res => {
                await this.setState({data: res.data['producoes']})
            })
       }else if(this.state.selected == 'projetos'){
        this.setState({data:{}, title: 'Projetos Vinculados'})
        await api.get('/projetos')
        .then(async res => {
            await this.setState({data: res.data['projetos']})
        })
        }else if(selected == 'user'){
            this.setState({data:{}, title: 'Usuários'})
            await api.get('/usuarios')
            .then(async res => {
                await this.setState({data: res.data['usuarios']})
            })
       }else if(selected == 'equipe'){
        this.setState({data:{}, title: 'Equipe'})
        await api.get('/equipe')
        .then(async res => {
            await this.setState({data: res.data['equipe']})
        })
   }else if(selected == 'parceiros'){
            this.setState({data:{}, title: 'Parceiros e Fomento'})
            await api.get('/parceiros')
            .then(async res => {
                await this.setState({data: res.data['parceiros']})
            })
       }else if(selected == 'municipios'){
            this.setState({data:{}, title: 'Municípios'})
            await api.get('/municipios')
            .then(async res => {
                await this.setState({data: res.data['municipios']})
            })
       }else if(selected == 'toponimias'){
            this.setState({data:{}, title: 'Toponímias'})
            await api.get('/toponimias')
            .then(async res => {
                await this.setState({data: res.data['toponimias']})
            })
       }
    };

    onToggle = (expanded) => {
        this.setState({ expanded: expanded });
    };

    renderBreadcrumbs() {
        const { selected } = this.state;
        var key = ''
    
        return (
            <>
            <div>
                <TableList selected={selected} data={this.state.data} title={this.state.title}/>
            </div>
            </>
        );
    }

    navigate = (pathname) => () => {
        this.setState({ selected: pathname });
    };

    render() {
        const { expanded, selected } = this.state;

        return (
            <div>
                <div
                    style={{
                        marginLeft: expanded ? 240 : 64,
                        padding: '15px 20px 0 20px'
                    }}
                >
                
                    
                </div>
                <SideNav onSelect={this.onSelect} onToggle={this.onToggle}>
                    <SideNav.Toggle />
                    <SideNav.Nav selected={selected}>
                        <NavItem eventKey="home">
                            <NavIcon>
                                <FaHome className='social-icon' size="20" color="#fff" />
                            </NavIcon>
                            <NavText style={{ paddingRight: 32 }} title="Home">
                                Home
                            </NavText>
                        </NavItem>
                        <NavItem eventKey="user">
                            <NavIcon>
                                <FaUserAlt className='social-icon' size="20" color="#fff" />
                            </NavIcon>
                            <NavText style={{ paddingRight: 32 }} title="Usuários">
                                Usuários
                            </NavText>
                        </NavItem>
                        <NavItem eventKey="equipe">
                            <NavIcon>
                                <FaUsers className='social-icon' size="20" color="#fff" />
                            </NavIcon>
                            <NavText style={{ paddingRight: 32 }} title="Equipe">
                                Equipe
                            </NavText>
                        </NavItem>
                        <NavItem eventKey="noticias">
                            <NavIcon>
                                <FaNewspaper className='social-icon' size="20" color="#fff" />
                            </NavIcon>
                            <NavText style={{ paddingRight: 32 }} title="Noticias">
                                Notícias
                            </NavText>
                        </NavItem>
                        <NavItem eventKey="producao">
                            <NavIcon>
                                <FaLightbulb className='social-icon' size="20" color="#fff" />
                            </NavIcon>
                            <NavText style={{ paddingRight: 32 }} title="Produção Científica">
                                Produção Intelectual
                            </NavText>
                        </NavItem>
                        <NavItem eventKey="projetos">
                            <NavIcon>
                                <FaProjectDiagram className='social-icon' size="20" color="#fff" />
                            </NavIcon>
                            <NavText style={{ paddingRight: 32 }} title="Projetos Vinculados">
                                Projetos Vinculados
                            </NavText>
                        </NavItem>
                        <NavItem eventKey="parceiros">
                            <NavIcon>
                                <FaRegHandshake className='social-icon' size="20" color="#fff" />
                            </NavIcon>
                            <NavText style={{ paddingRight: 32 }} title="Produção Científica">
                                Parceiros e Fomento
                            </NavText>
                        </NavItem>
                        <NavItem eventKey="municipios">
                            <NavIcon>
                                <FaRegMap className='social-icon' size="20" color="#fff" />
                            </NavIcon>
                            <NavText style={{ paddingRight: 32 }} title="Municípios">
                                Municípios
                            </NavText>
                        </NavItem>
                        <NavItem eventKey="toponimias">
                            <NavIcon>
                                <FaMapMarkerAlt className='social-icon' size="20" color="#fff" />
                            </NavIcon>
                            <NavText style={{ paddingRight: 32 }} title="Toponímias">
                                Toponímias
                            </NavText>
                        </NavItem>
                        <NavItem eventKey="logout" onClick={this.handleLogout}>
                            <NavIcon>
                                <FaSignOutAlt className='social-icon' size="20" color="#fff" />
                            </NavIcon>
                            <NavText style={{ paddingRight: 32 }} title="Reports">
                                Sair
                            </NavText>
                        </NavItem>
                        <NavItem eventKey="settings">
                            <NavIcon>
                                <i className="fa fa-fw fa-cogs" style={{ fontSize: '1.5em', verticalAlign: 'middle' }} />
                            </NavIcon>
                            <NavText style={{ paddingRight: 32 }} title="Settings">
                                Settings
                            </NavText>
                            <NavItem eventKey="settings/policies">
                                <NavText title="Policies">
                                    Policies
                                </NavText>
                            </NavItem>
                            <NavItem eventKey="settings/network">
                                <NavText title="Network">
                                    Network
                                </NavText>
                            </NavItem>
                        </NavItem>
                    </SideNav.Nav>
                </SideNav>
                <Main expanded={expanded}>
                    {this.renderBreadcrumbs()}
                </Main>
            </div>
        );
    }
}