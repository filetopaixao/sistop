import React, {useState, useMemo, useEffect } from 'react'
import api from '../../../services/api'
var createObjectURL = require('create-object-url');

const FormEquipeEdit = (props) => {

    const [nome, setNome ] = useState(props.nome)
    const [formacao, setFormacao ] = useState(props.formacao)
    const [atuacao, setAtuacao ] = useState(props.atuacao)
    const [cargo, setCargo ] = useState(props.cargo)
    const [lattes, setLattes ] = useState(props.lattes)
    const [email, setEmail ] = useState(props.email)
    const [telefone, setTelefone ] = useState(props.telefone)
    const [facebook, setFacebook ] = useState(props.facebook)
    const [instagram, setInstagram ] = useState(props.instagram)
    const [linkedin, setLinkedin ] = useState(props.linkedin)
    const [twitter, setTwitter ] = useState(props.twitter)
    const [skype, setSkype ] = useState(props.skype)
    const [image, setImage ] = useState(props.image)
    const [file, setFile] = useState([])

    useEffect(()=>{
        setNome(props.nome)
        setFormacao(props.formacao)
        setAtuacao(props.atuacao)
        setCargo(props.cargo)
        setLattes(props.lattes)
        setEmail(props.email)
        setTelefone(props.telefone)
        setFacebook(props.facebook)
        setInstagram(props.instagram)
        setLinkedin(props.linkedin)
        setTwitter(props.twitter)
        setSkype(props.skype)
        setImage(props.image)
    },[props.atuacao, props.cargo, props.email, props.facebook, props.formacao, props.image, props.instagram, props.lattes, props.linkedin, props.nome, props.skype, props.telefone, props.toponimia, props.twitter])

    const preview = useMemo(() => {
        return typeof image != 'string' ? createObjectURL(image) : image;
      }, [image])


    const uploadFiles = (e) => {
        e.preventDefault()
        console.log(file)
    }

    const handleUsuario = async (e) => {
        e.preventDefault()

        const idUser = localStorage.getItem('userId')

        const data = new FormData();

        data.append('image', image);
        data.append('nome', nome);
        data.append('formacao', formacao);
        data.append('atuacao', atuacao);
        data.append('cargo', cargo);
        data.append('lattes', lattes);
        data.append('email', email);
        data.append('telefone', telefone);
        data.append('facebook', facebook);
        data.append('instagram', instagram);
        data.append('linkedin', linkedin);
        data.append('twitter', twitter);
        data.append('skype', skype);

        try{
            await api.put('/equipe/'+ props.id, data)

            setNome('')
            setFormacao('')
            setAtuacao('')
            setCargo('')
            setLattes('')
            setEmail('')
            setTelefone('')
            setFacebook('')
            setInstagram('')
            setLinkedin('')
            setTwitter('')
            setSkype('')
            setImage('')
            
            alert('Integrante alterado com sucesso!')
        }catch(err){
            alert('Erro ao alterar Integrante, tente novamente.')
        }
    }

    const deleteUsuario = async (e) => {
        e.preventDefault()
        const r = window.confirm("Do you really want to delete it?");
        if(r == true){
            try{
                await api.delete('/equipe/' + props.id)
                
                alert('Integrante deletado com sucesso!')
            }catch(err){
                alert('Erro ao deletar o Integrante, tente novamente.')
            }
            //this.props.history.push('/admin')
        }
        //props.id
        
        
    }


    return(
        <form onSubmit={handleUsuario}>
            <div className="form-group">
                <label for="formGroupExampleInput2">Nome Completo</label>
                <input type="text" className="form-control" placeholder="Nome do Integrante" value={nome} onChange={ e => setNome(e.target.value)}/>
            </div>
            <div className="form-group">
                <div><label for="formGroupExampleInput2">Imagem Destaque</label></div>
                <label 
                    id="thumbnail" 
                    style={{ backgroundImage: `url(${preview})`, width: '100%', height: '200px', backgroundSize: 'contain', backgroundRepeat: 'no-repeat' }}
                    className={image ? 'has-thumbnail' : ''}
                >
                    
                </label><input type="file" onChange={event => setImage(event.target.files[0])} name="gallery"/>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Formação</label>
                <input type="text" className="form-control" placeholder="Formação do Integrante" value={formacao} onChange={ e => setFormacao(e.target.value)}/>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Atuação</label>
                <input type="text" className="form-control" placeholder="Atuação do Integrante" value={atuacao} onChange={ e => setAtuacao(e.target.value)}/>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Cargo</label>
                <input type="text" className="form-control" placeholder="Cargo do Integrante" value={cargo} onChange={ e => setCargo(e.target.value)}/>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Lattes</label>
                <input type="text" className="form-control" placeholder="Lattes do Integrante" value={lattes} onChange={ e => setLattes(e.target.value)}/>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Email</label>
                <input type="text" className="form-control" placeholder="Email do Integrante" value={email} onChange={ e => setEmail(e.target.value)}/>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Telefone</label>
                <input type="text" className="form-control" placeholder="Telefone do Integrante" value={telefone} onChange={ e => setTelefone(e.target.value)}/>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Facebook</label>
                <input type="text" className="form-control" placeholder="Facebook do Integrante" value={facebook} onChange={ e => setFacebook(e.target.value)}/>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Instagram</label>
                <input type="text" className="form-control" placeholder="Instagram do Integrante" value={instagram} onChange={ e => setInstagram(e.target.value)}/>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">LinkedIn</label>
                <input type="text" className="form-control" placeholder="LinkedIn do Integrante" value={linkedin} onChange={ e => setLinkedin(e.target.value)}/>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Twitter</label>
                <input type="text" className="form-control" placeholder="Twitter do Integrante" value={twitter} onChange={ e => setTwitter(e.target.value)}/>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Skype</label>
                <input type="text" className="form-control" placeholder="Skype do Integrante" value={skype} onChange={ e => setSkype(e.target.value)}/>
            </div>
                
            
            <button type="submit" className="btn btn-primary" style={{marginRight: '15px'}}>Atualizar</button>
            <button onClick={deleteUsuario} className="btn btn-danger">Deletar</button>
        </form>
    )
}

export default FormEquipeEdit