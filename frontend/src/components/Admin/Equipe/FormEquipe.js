import React, {useState, useMemo } from 'react'
import api from '../../../services/api'
var createObjectURL = require('create-object-url');

const FormEquipe = (props) => {
    var fileObj = [];
    var fileArray = [];

    const [nome, setNome ] = useState('')
    const [formacao, setFormacao ] = useState('')
    const [atuacao, setAtuacao ] = useState('')
    const [cargo, setCargo ] = useState('')
    const [lattes, setLattes ] = useState('')
    const [email, setEmail ] = useState('')
    const [telefone, setTelefone ] = useState('')
    const [facebook, setFacebook ] = useState('')
    const [instagram, setInstagram ] = useState('')
    const [linkedin, setLinkedin ] = useState('')
    const [twitter, setTwitter ] = useState('')
    const [skype, setSkype ] = useState('')
    const [image, setImage ] = useState(null)
    const [file, setFile] = useState([])

    const preview = useMemo(() => {
        return image ? createObjectURL(image) : null;
      }, [image])


    const uploadFiles = (e) => {
        e.preventDefault()
        console.log(file)
    }

    const handleUsuario = async (e) => {
        e.preventDefault()

        const idUser = localStorage.getItem('userId')

        const data = new FormData();

        data.append('image', image);
        data.append('nome', nome);
        data.append('formacao', formacao);
        data.append('atuacao', atuacao);
        data.append('cargo', cargo);
        data.append('lattes', lattes);
        data.append('email', email);
        data.append('telefone', telefone);
        data.append('facebook', facebook);
        data.append('instagram', instagram);
        data.append('linkedin', linkedin);
        data.append('twitter', twitter);
        data.append('skype', skype);
        
        /*
        var data = {
            nome,
            formacao,
            atuacao,
            cargo,
            lattes,
            email,
            telefone,
            facebook,
            instagram,
            linkedin,
            twitter,
            skype
        }
        */

        try{
            await api.post('/equipe', data, {
                headers: {
                    Authorization: idUser
                }
            })

            setNome('')
            setFormacao('')
            setAtuacao('')
            setCargo('')
            setLattes('')
            setEmail('')
            setTelefone('')
            setFacebook('')
            setInstagram('')
            setLinkedin('')
            setTwitter('')
            setSkype('')
            setImage('')
            
            alert('Integrante cadastrada com sucesso!')
        }catch(err){
            alert('Erro ao cadastrar Integrante, tente novamente.')
        }
    }


    return(
        <form onSubmit={handleUsuario}>
            <div className="form-group">
                <label for="formGroupExampleInput2">Nome Completo</label>
                <input type="text" className="form-control" placeholder="Nome do Integrante" value={nome} onChange={ e => setNome(e.target.value)}/>
            </div>
            <div className="form-group">
                <div><label for="formGroupExampleInput2">Imagem Destaque</label></div>
                <label 
                    id="thumbnail" 
                    style={{ backgroundImage: `url(${preview})`, width: '100%', height: '200px', backgroundSize: 'contain', backgroundRepeat: 'no-repeat' }}
                    className={image ? 'has-thumbnail' : ''}
                >
                    
                </label><input type="file" onChange={event => setImage(event.target.files[0])} name="gallery"/>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Formação</label>
                <input type="text" className="form-control" placeholder="Formação do Integrante" value={formacao} onChange={ e => setFormacao(e.target.value)}/>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Atuação</label>
                <input type="text" className="form-control" placeholder="Atuação do Integrante" value={atuacao} onChange={ e => setAtuacao(e.target.value)}/>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Cargo</label>
                <input type="text" className="form-control" placeholder="Cargo do Integrante" value={cargo} onChange={ e => setCargo(e.target.value)}/>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Lattes</label>
                <input type="text" className="form-control" placeholder="Lattes do Integrante" value={lattes} onChange={ e => setLattes(e.target.value)}/>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Email</label>
                <input type="text" className="form-control" placeholder="Email do Integrante" value={email} onChange={ e => setEmail(e.target.value)}/>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Telefone</label>
                <input type="text" className="form-control" placeholder="Telefone do Integrante" value={telefone} onChange={ e => setTelefone(e.target.value)}/>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Facebook</label>
                <input type="text" className="form-control" placeholder="Facebook do Integrante" value={facebook} onChange={ e => setFacebook(e.target.value)}/>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Instagram</label>
                <input type="text" className="form-control" placeholder="Instagram do Integrante" value={instagram} onChange={ e => setInstagram(e.target.value)}/>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">LinkedIn</label>
                <input type="text" className="form-control" placeholder="LinkedIn do Integrante" value={linkedin} onChange={ e => setLinkedin(e.target.value)}/>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Twitter</label>
                <input type="text" className="form-control" placeholder="Twitter do Integrante" value={twitter} onChange={ e => setTwitter(e.target.value)}/>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Skype</label>
                <input type="text" className="form-control" placeholder="Skype do Integrante" value={skype} onChange={ e => setSkype(e.target.value)}/>
            </div>
                
            
            <button type="submit" className="btn btn-primary">Cadastrar</button>
        </form>
    )
}

export default FormEquipe