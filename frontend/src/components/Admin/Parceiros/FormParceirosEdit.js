import React, {useState, useMemo } from 'react'
import api from '../../../services/api'
var createObjectURL = require('create-object-url');

const FormParceirosEdit = (props) => {
    var fileObj = [];
    var fileArray = [];
    const idUser = localStorage.getItem('userId')

    const [name, setName ] = useState(props.name)
    const [category, setCategory ] = useState(props.category)
    const [link, setLink ] = useState(props.link)
    const [image, setImage ] = useState(props.image)
    const [file, setFile] = useState([])

    const preview = useMemo(() => {
        return typeof image != 'string' ? createObjectURL(image) : image;
      }, [image])

      const uploadMultipleFiles = (e) => {
        fileObj.push(e.target.files)
        for (let i = 0; i < fileObj[0].length; i++) {
            fileArray.push(URL.createObjectURL(fileObj[0][i]))
        }
        setFile( fileArray )
    }

    const uploadFiles = (e) => {
        e.preventDefault()
        console.log(file)
    }

    const handleSubmit = async (e) => {
        e.preventDefault()

        if(typeof image == 'string' ){
            var data = {
                name,
                category,
                link,
                image: image
            }
        }else{
            var data = new FormData();

            data.append('image', image);
            //data2.append('gallery', file);
            data.append('name', name);
            data.append('category', category);
            data.append('link', link);
        }

        try{
            await api.put('/parceiros/' + props.id, data)
            
            alert('Parceiro / Fomento atualizada com sucesso!')
        }catch(err){
            alert('Erro ao cadastrar Parceiro / Fomento, tente novamente.')
        }
    }

    const handleDelete = async (e) => {
        e.preventDefault()
        const r = window.confirm("Do you really want to delete it?");
        if(r == true){
            try{
                await api.delete('/parceiros/' + props.id)
                
                alert('Parceiro / Fomento deletado com sucesso!')
            }catch(err){
                alert('Erro ao deletar o Parceiro / Fomento, tente novamente.')
            }
        }
        
    }

    return(
        <form onSubmit={handleSubmit}>
            <div className="form-group">
                <label for="formGroupExampleInput2">Nome</label>
                <input type="text" className="form-control" placeholder="Nome do Parceiro / Fomento" value={name} onChange={ e => setName(e.target.value)}/>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Categoria</label>
                <select class="form-control" id="exampleFormControlSelect1" value={category} onChange={ e => setCategory(e.target.value)}>
                    <option value="Parceiro">Parceiro</option>
                    <option value="Fomento">Fomento</option>
                </select>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Link</label>
                <input type="text" className="form-control" id="formGroupExampleInput2" placeholder="Link do Parceiro / Fomento" value={link} onChange={ e => setLink(e.target.value)} />
            </div>

            <div className="form-group">
                <div><label for="formGroupExampleInput2">Logo do Parceiro / Fomento</label></div>
                <label 
                    id="thumbnail" 
                    style={{ backgroundImage: `url(${preview})`, width: '100%', height: '100px', backgroundSize: 'contain', backgroundRepeat: 'no-repeat' }}
                    className={image ? 'has-thumbnail' : ''}
                >
                    
                </label><input type="file" onChange={event => setImage(event.target.files[0])} name="gallery"/>
            </div>
                
            
            <button type="submit" className="btn btn-primary" style={{marginRight: '15px'}}>Atualizar</button>
            <button onClick={handleDelete} className="btn btn-danger">Deletar</button>
        </form>
    )
}

export default FormParceirosEdit