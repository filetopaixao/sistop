import React, {useState, useMemo } from 'react'
import api from '../../../services/api'
//import MultiUpload from './MultiUpload'
var createObjectURL = require('create-object-url');

const FormParceiros = (props) => {
    var fileObj = [];
    var fileArray = [];
    const idUser = localStorage.getItem('userId')
    
    const [name, setName ] = useState('')
    const [category, setCategory ] = useState('Parceiro')
    const [link, setLink ] = useState('')
    const [image, setImage ] = useState(null)
    const [file, setFile] = useState([])

    const preview = useMemo(() => {
        return image ? createObjectURL(image) : null;
      }, [image])

      const uploadMultipleFiles = (e) => {
        fileObj.push(e.target.files)
        for (let i = 0; i < fileObj[0].length; i++) {
            fileArray.push(URL.createObjectURL(fileObj[0][i]))
        }
        setFile( fileArray )
    }

    const uploadFiles = (e) => {
        e.preventDefault()
        console.log(file)
    }

    const handleNews = async (e) => {
        e.preventDefault()

        
        const data = new FormData();

        data.append('image', image);
        data.append('name', name);
        data.append('category', category);
        data.append('link', link);

        try{
            await api.post('/parceiros', data, {
                headers: {
                    Authorization: idUser
                }
            })

            setName('')
            setCategory('')
            setLink('')
            setImage('')
            
            alert('Parceiro / Fomento cadastrado com sucesso!')
        }catch(err){
            alert('Erro ao cadastrar Parceiro / Fomento, tente novamente.')
        }
    }


    return(
        <form onSubmit={handleNews}>
            <div className="form-group">
                <label for="formGroupExampleInput2">Nome</label>
                <input type="text" className="form-control" placeholder="Nome do Parceiro / Fomento" value={name} onChange={ e => setName(e.target.value)}/>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Categoria</label>
                <select class="form-control" id="exampleFormControlSelect1" value={category} onChange={ e => setCategory(e.target.value)}>
                    <option value="Parceiro">Parceiro</option>
                    <option value="Fomento">Fomento</option>
                </select>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Link</label>
                <input type="text" className="form-control" id="formGroupExampleInput2" placeholder="Link do Parceiro / Fomento" value={link} onChange={ e => setLink(e.target.value)} />
            </div>

            <div className="form-group">
                <div><label for="formGroupExampleInput2">Logo do Parceiro / Fomento</label></div>
                <label 
                    id="thumbnail" 
                    style={{ backgroundImage: `url(${preview})`, width: '100%', height: '100px', backgroundSize: 'contain', backgroundRepeat: 'no-repeat' }}
                    className={image ? 'has-thumbnail' : ''}
                >
                    
                </label><input type="file" onChange={event => setImage(event.target.files[0])} name="gallery"/>
            </div>
                
            
            <button type="submit" className="btn btn-primary">Cadastrar</button>
        </form>
    )
}

export default FormParceiros