import React, {useState, useEffect} from 'react'
import { Link } from 'react-router-dom'
import Slider from "react-slick";
import {TeamMemberPhoto, TeamMemberPhotoOverlay, TeamMemberCard} from '../../../styles'
import { FaFacebookSquare, FaInstagram, FaSkype, FaLinkedin, FaTwitterSquare } from "react-icons/fa";


const SliderTeam = (props) => {
    const [propLength ,  setPropLength] = useState(3)

    useEffect( () => {
      setPropLength(props.team.length)
    })

    var settings = {
        dots: false,
        infinite: true,
        speed: 1000,
        slidesToShow: propLength < 3 ? propLength : 3,
        slidesToScroll: 1,
        initialSlide: 0,
        autoplay:true,
        autoplaySpeed: 5000,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 1,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1,
              initialSlide: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
      };

      return(
        <Slider className='team-members' {...settings} arrows={true}>
            {
                props.team.map(member => (
                <div>
                    <TeamMemberCard className='item'>
                        <TeamMemberPhoto className="photo" photo={member.foto} />
                        <TeamMemberPhotoOverlay className='photo-overlay'>
                        {member.facebook != 'null' && member.facebook ? (<a href={member.facebook} target="_blank"><div><FaFacebookSquare className='social-icon' size="16" color="#BF1E2D" /></div></a>) : ''}
                        {member.instagram != 'null' && member.instagram ? (<a href={member.instagram} target="_blank"><div><FaInstagram className='social-icon' size="16" color="#BF1E2D" /></div></a>) : ''}
                        {member.skype != 'null' && member.skype? (<a href={member.skype} target="_blank"><div><FaSkype className='social-icon' size="16" color="#BF1E2D" /></div></a>) : ''}
                        {member.linkedin != 'null' && member.linkedin ? (<a href={member.linkedin} target="_blank"><div><FaLinkedin className='social-icon' size="16" color="#BF1E2D" /></div></a>) : ''}
                        {member.twitter != 'null' && member.twitter ? (<a href={member.twitter} target="_blank"><div><FaTwitterSquare className='social-icon' size="16" color="#BF1E2D" /></div></a>) : ''}
                        </TeamMemberPhotoOverlay>
                        <h5>{member.nome}</h5>
                        <p>{member.formacao}</p>
                        <Link className="btn btn-primary button" style={{}}>
                        Saiba mais
                    </Link>
                    </TeamMemberCard>
                </div>
                )
                )
            }
            
        </Slider>
      )
    }

    export default SliderTeam