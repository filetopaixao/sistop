import React, {useState, useEffect} from 'react'
import {Section} from '../../../styles'
import SliderTeam from '../../../components/Portal/Equipe/SliderTeam'
import api from '../../../services/api'

const Equipe = () => {

    const [equipe, setEquipe] = useState([])

    useEffect(async () =>{
      const res = await api.get('/equipe')
      setEquipe(res.data['equipe'])
    }, [])

      
    return(
        <div style={{ backgroundColor:'#f8f9fa'}}>
            <Section className="container" id="equipe">
                <h2>Equipe</h2>

                <SliderTeam team={equipe} len={equipe.length} />

            </Section>
        </div>
    )
}

export default Equipe