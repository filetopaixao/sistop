import React, {useState, useEffect} from 'react'
import {Section} from '../../../styles'
import SliderPartner from '../../../components/Portal/Parceiros/SliderPartner'
import api from '../../../services/api'

const Parceiros = () => {
  const [parceiros, setParceiros] = useState([])
  const [fomento, setFomento] = useState([])

  useEffect(async () =>{
    var p =[]
    var f = []
    const res = await api.get('/parceiros')
    res.data['parceiros'].map( item => {
        if(item.category == 'Parceiro'){
          p = [...p, item]
        }else if(item.category == 'Fomento'){
          f = [...f, item]
        }
    })
    
    setParceiros(p)
    setFomento(f)
  }, [])

    return(
        <div style={{ backgroundColor:'#fff'}}>
            <Section className="container" id="parceiros">
                <div className='row'>
                  <div className='col-md-5'>
                    <h2>Parceiros</h2>
                    <SliderPartner partners={parceiros} />
                  </div>
                  <div className="col-md-2" />
                  <div className='col-md-5'>
                    <h2>Fomento</h2>
                    <SliderPartner partners={fomento} />
                  </div>
                </div>
            </Section>
        </div>
    )
}

export default Parceiros