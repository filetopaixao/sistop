import React, {useEffect, useState} from 'react'
import api from '../../../../services/api'

//import { GoogleMap, LoadScript } from '@react-google-maps/api';

import 'ol/ol.css';
import Map from 'ol/Map';
import View from 'ol/View';
import {Image as ImageLayer, Tile as TileLayer,  Vector as VectorLayer} from 'ol/layer';
import ImageWMS from 'ol/source/ImageWMS';
import OSM from 'ol/source/OSM';
import './ol.css';
import Feature from 'ol/Feature';
import {Icon, Style} from 'ol/style';
import Point from 'ol/geom/Point';
import {fromLonLat, transform} from 'ol/proj';
import VectorSource from 'ol/source/Vector';

import Modal from '../../../../components/Portal/Projeto/Modal';

const MapTO = () => {

  //const [features, setFeatures] = useState([])
  const [comInd, setComInd] = useState(0)
  const [comRem, setComRem] = useState(0)
  const [rios, setRios] = useState(0)
  const [modalTitle, setModalTitle] = useState('')
  const [modalContent, setModalContent] = useState('')


  useEffect( async () => {
      const script = document.createElement("script");

      script.src = "https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.3.1/build/ol.js";
      script.async = true;
      document.body.appendChild(script);
      var features = []
      await api.get('/toponimias')
      .then(res => {
          res.data['toponimias'].map((toponimia)=>{

            var marker = new Feature({
              geometry: new Point(fromLonLat([toponimia.long, toponimia.lat])),
              name: toponimia.nome+toponimia.id,
            });

            marker.setStyle(
              new Style({
                image: new Icon({
                  color: '#ff00ee',
                  crossOrigin: 'anonymous',
                  // For Internet Explorer 11
                  imgSize: [30, 30],
                  src: 'https://upload.wikimedia.org/wikipedia/commons/8/88/Map_marker.svg',
                }),
              })
            );

            features.push(marker)
            
          })
      })

        var wmsSource = new ImageWMS({
          url: 'http://localhost:8080/geoserver/tocantins/wms',
          params: {'LAYERS': 'tocantins:city', 'VERSION': '1.1.1', },
          ratio: 1,
          serverType: 'geoserver'
        });
        
        var wmsLayer = new ImageLayer({
          source: wmsSource,
        });

        var vectorSource = new VectorSource({
          features: features,
        });

        var vectorLayer = new VectorLayer({
          source: vectorSource,
        });

        var view = new View({
          center: transform([-47.48, -9.45], 'EPSG:4326', 'EPSG:3857'),
          zoom: 6
        });

       var map = new Map({
          layers: [wmsLayer, vectorLayer],
          target: 'mapTO',
          view: view
        });

        map.on('singleclick', function (evt) {
          window.$('#modalPortal').modal('show');
          var viewResolution = /** @type {number} */ (view.getResolution());
          var url = wmsSource.getFeatureInfoUrl(
            evt.coordinate,
            viewResolution,
            'EPSG:3857',
            {'INFO_FORMAT': 'text/xml'}
          );
          if (url) {
            fetch(url)
            .then(function (response) { return response.text(); })
            .then(function (html) {
              console.log(html.split('tocantins:nome>').splice(1, 1))
            });
          }
        });
         
    }, [])

    return(
     <>
       < div id="mapTO" className="mapTO" style={{width: '100%', height: '400px'}}></div>
       <Modal id="modalPortal" title={modalTitle} content={modalContent} />
     </>
    )
}

export default MapTO