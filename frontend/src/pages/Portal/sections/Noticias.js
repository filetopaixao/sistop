import React, {useState, useEffect} from 'react'
import {Section} from '../../../styles'
import SliderNews from '../../../components/Portal/Noticias/SliderNews'
import Photo from '../../../assets/post.jpg'
import api from '../../../services/api'

const Noticias = () => {

      const [noticias, setNoticias] = useState([])

      useEffect(async () =>{
        const res = await api.get('/noticias')
        setNoticias(res.data['noticias'])
      }, [])

    return(
        <div style={{ backgroundColor:'#f8f9fa'}}>
            <Section className="container" id="noticias">
                <h2>Notícias</h2>

                <SliderNews news={noticias} len={noticias.length} />
            </Section>
        </div>
    )
}

export default Noticias