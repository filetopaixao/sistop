import React, {useEffect, useState} from 'react'
import DataTable from 'react-data-table-component';
import Modal from '../../../components/Admin/Modal'
import ReactDOM from 'react-dom';
//forms
import FormNoticias from '../../../components/Admin/Noticias/FormNoticias'
import FormNoticiasEdit from '../../../components/Admin/Noticias/FormNoticiasEdit'
import FormProducoes from '../../../components/Admin/Producoes/FormProducoes'
import FormProducoesEdit from '../../../components/Admin/Producoes/FormProducoesEdit'
import FormUsuarios from '../../../components/Admin/Usuarios/FormUsuarios'
import FormUsuariosEdit from '../../../components/Admin/Usuarios/FormUsuariosEdit'
import FormParceiros from '../../../components/Admin/Parceiros/FormParceiros'
import FormParceirosEdit from '../../../components/Admin/Parceiros/FormParceirosEdit'
import FormEquipe from '../../../components/Admin/Equipe/FormEquipe'
import FormEquipeEdit from '../../../components/Admin/Equipe/FormEquipeEdit'
import FormToponimias from '../../../components/Admin/Toponimias/FormToponimias'
import FormToponimiasEdit from '../../../components/Admin/Toponimias/FormToponimiasEdit'
import FormMunicipios from '../../../components/Admin/Municipios/FormMunicipios'
import FormMunicipiosEdit from '../../../components/Admin/Municipios/FormMunicipiosEdit'

import 'ol/ol.css';
import Map from 'ol/Map';
import View from 'ol/View';
import {Image as ImageLayer, Tile as TileLayer,  Vector as VectorLayer} from 'ol/layer';
import ImageWMS from 'ol/source/ImageWMS';
import OSM from 'ol/source/OSM';
import {transform} from 'ol/proj';

import api from '../../../services/api'
var dateFormat = require('dateformat');



export default class TableList extends React.Component{
    state = {
      title: '',
      nomeMunicipio: ''
    }

    handleCadastro = () => {
        if(this.props.selected == 'noticias'){
            ReactDOM.render(
                <FormNoticias />,
                document.getElementById('modal-body')
              );
        }
    }


    clicked = async (e) => {
      console.log(e.id)
      if(e.id){
        if(this.props.selected == 'noticias'){
          var title, subtitle, author, date, image, content = ''
          window.$('#exampleModal').modal('show');
          await api.get('/noticias')
            .then(async res => {
              res.data.noticias.map( noticia => {
                  if(noticia.id == e.id){
                    console.log('Achou a noticia', e.id)
                    title = noticia.title
                    subtitle = noticia.subtitle
                    author = noticia.author
                    date = dateFormat(noticia.news_date, "yyyy-mm-dd")
                    image = noticia.image
                    content = noticia.content_text
                  }
                })
            })
          ReactDOM.render(
            <FormNoticiasEdit id={e.id} title={title} subtitle={subtitle} author={author} date={date} image={image} content={content} />,
            document.getElementById('modal-body')
          );

        }else if(this.props.selected == 'producao'){
          var title, category, year, link, description = ''
          window.$('#exampleModal').modal('show');
          await api.get('/producoes')
            .then(async res => {
              res.data.producoes.map( producao => {
                  if(producao.id == e.id){
                    title = producao.title
                    category = producao.category
                    year = producao.year
                    link = producao.link
                    description = producao.description
                  }
                })
            })
          ReactDOM.render(
            <FormProducoesEdit id={e.id} title={title} category={category} year={year} link={link} image={image} description={description} />,
            document.getElementById('modal-body')
          );
        }else if(this.props.selected == 'user'){
          var name, username, password, role = ''
          window.$('#exampleModal').modal('show');
          await api.get('/usuarios')
            .then(async res => {
              res.data.usuarios.map( user => {
                  if(user.id == e.id){
                    name = user.name
                    username = user.username
                    password = user.password
                    role = user.role
                  }
                })
            })
          ReactDOM.render(
            <FormUsuariosEdit id={e.id} name={name} username={username} password={password} role={role} />,
            document.getElementById('modal-body')
          );
        }else if(this.props.selected == 'equipe'){
          var nome, formacao, atuacao, cargo, lattes, email, telefone, facebook, instagram, linkedin, twitter, skype, published_date, foto = ''
          window.$('#exampleModal').modal('show');
          await api.get('/equipe')
            .then(async res => {
              res.data.equipe.map( integrante => {
                  if(integrante.id == e.id){
                    nome = integrante.nome
                    formacao = integrante.formacao
                    cargo = integrante.cargo
                    atuacao = integrante.atuacao
                    lattes = integrante.lattes
                    email = integrante.email
                    telefone = integrante.telefone
                    facebook = integrante.facebook
                    instagram = integrante.instagram
                    linkedin = integrante.linkedin
                    twitter = integrante.twitter
                    skype = integrante.skype
                    published_date = integrante.published_date
                    foto = integrante.foto
                  }
                })
            })
          ReactDOM.render(
            <FormEquipeEdit id={e.id} nome={nome} formacao={formacao} atuacao={atuacao} cargo={cargo} lattes={lattes} 
            email={email} telefone={telefone} facebook={facebook} instagram={instagram} linkedin={linkedin} 
            twitter={twitter} skype={skype} published_date={published_date} image={foto} />,
            document.getElementById('modal-body')
          );
        }else if(this.props.selected == 'parceiros'){
          var name, image, category, link, published_date = ''
          window.$('#exampleModal').modal('show');
          await api.get('/parceiros')
            .then(async res => {
              res.data.parceiros.map( parceiro => {
                  if(parceiro.id == e.id){
                    name = parceiro.name
                    category = parceiro.category
                    published_date = dateFormat(parceiro.published_date, "yyyy-mm-dd")
                    image = parceiro.image
                    link = parceiro.link
                  }
                })
            })
          ReactDOM.render(
            <FormParceirosEdit id={e.id} name={name} category={category} image={image} link={link} />,
            document.getElementById('modal-body')
          );

        }else if(this.props.selected == 'toponimias'){
          var nome, long, lat, categoria, image, toponimiaDesc = ''
          window.$('#exampleModal').modal('show');
          await api.get('/toponimias')
            .then(async res => {
              res.data.toponimias.map( toponimia => {
                  if(toponimia.id == e.id){
                    nome = toponimia.nome
                    long = toponimia.long
                    lat = toponimia.lat
                    categoria = toponimia.categoria
                    image = toponimia.image
                    toponimiaDesc = toponimia.toponimia
                  }
                })
            })
          ReactDOM.render(
            <FormToponimiasEdit id={e.id} nome={nome} long={long} lat={lat} image={image} categoria={categoria} toponimia={toponimiaDesc} />,
            document.getElementById('modal-body')
          );

          }else if(this.props.selected == 'municipios'){
            var nome, image, toponimia = ''
          window.$('#exampleModal').modal('show');
          console.log(e.id)
          await api.get('/municipios')
            .then(async res => {
              res.data.municipios.map( municipio => {
                  if(municipio.id == e.id){
                    nome = municipio.nome
                    image = municipio.image
                    toponimia = municipio.toponimia
                  }
                })
            })
          ReactDOM.render(
            <FormMunicipiosEdit id={e.id} nome={nome} image={image} toponimia={toponimia} />,
            document.getElementById('modal-body')
          );

        }
      }else{
        window.$('#exampleModal').modal('show');
        if(this.props.selected == 'noticias'){
          ReactDOM.render(
            <FormNoticias />,
            document.getElementById('modal-body')
          );
        }else if(this.props.selected == 'producao'){
          ReactDOM.render(
            <FormProducoes />,
            document.getElementById('modal-body')
          );
        }else if(this.props.selected == 'user'){
          ReactDOM.render(
            <FormUsuarios />,
            document.getElementById('modal-body')
          );
        }else if(this.props.selected == 'equipe'){
          ReactDOM.render(
            <FormEquipe />,
            document.getElementById('modal-body')
          );
        }else if(this.props.selected == 'parceiros'){
          ReactDOM.render(
            <FormParceiros />,
            document.getElementById('modal-body')
          );
        }else if(this.props.selected == 'toponimias'){
          ReactDOM.render(
            <FormToponimias />,
            document.getElementById('modal-body')
          );
        }else if(this.props.selected == 'municipios'){
          ReactDOM.render(
            <FormMunicipios />,
            document.getElementById('modal-body')
          );
        }
      }
    }


    render() {
        if(this.props.selected == 'noticias'){
          var columns = [
            {
              name: 'Id',
              selector: 'id',
              sortable: true,
              width: '50px'
            },
            {
              name: 'Título',
              selector: 'title',
              sortable: true,
            },
            ,
            {
              name: 'Autor',
              selector: 'author',
              sortable: true,
            },
            ,
            {
              name: 'Data de Publicação',
              selector: 'published_date',
              sortable: true,
              format: row => dateFormat(row.news_date, "dd/mm/yyyy")
            },
          ];
        }else if(this.props.selected == 'producao'){
          var columns = [
            {
              name: 'Id',
              selector: 'id',
              sortable: true,
              width: '50px'
            },
            {
              name: 'Título',
              selector: 'title',
              sortable: true,
            },
            ,
            {
              name: 'Categoria',
              selector: 'category',
              sortable: true,
            },
            ,
            {
              name: 'Data de Publicação',
              selector: 'published_date',
              sortable: true,
              format: row => dateFormat(row.news_date, "dd/mm/yyyy")
            },
          ];
        }else if(this.props.selected == 'user'){
          var columns = [
            {
              name: 'Id',
              selector: 'id',
              sortable: true,
              width: '50px'
            },
            {
              name: 'Nome',
              selector: 'name',
              sortable: true,
            },
            {
              name: 'Usuário',
              selector: 'username',
              sortable: true,
            },
            {
              name: 'Função',
              selector: 'role',
              sortable: true,
            },
          ];
        }else if(this.props.selected == 'equipe'){
          var columns = [
            {
              name: 'Id',
              selector: 'id',
              sortable: true,
              width: '50px'
            },
            {
              name: 'Nome',
              selector: 'nome',
              sortable: true,
            },
            {
              name: 'Email',
              selector: 'email',
              sortable: true,
            },
            {
              name: 'Cargo',
              selector: 'cargo',
              sortable: true,
            },
          ];
        }else if(this.props.selected == 'parceiros'){
          var columns = [
            {
              name: 'Id',
              selector: 'id',
              sortable: true,
              width: '50px'
            },
            {
              name: 'Nome',
              selector: 'name',
              sortable: true,
            },
            {
              name: 'Categoria',
              selector: 'category',
              sortable: true,
            },
            {
              name: 'Data de Publicação',
              selector: 'published_date',
              sortable: true,
            },
          ];
        }else if(this.props.selected == 'toponimias'){
          var columns = [
            {
              name: 'Id',
              selector: 'id',
              sortable: true,
              width: '50px'
            },
            {
              name: 'Categoria',
              selector: 'categoria',
              sortable: true,
            },
          ];
        }else if(this.props.selected == 'municipios'){
          var columns = [
            {
              name: 'Código',
              selector: 'codigo_ibg',
              sortable: true,
              width: '100px'
            },
            {
              name: 'Nome',
              selector: 'nome',
              sortable: true,
              width: '50%'
            },
            {
              name: 'Toponímia',
              selector: 'toponimia',
              sortable: true,
              width: '50%'
            }
          ];
        }
        

        if(this.props.selected == 'home'){
            return (<h1>As</h1>)
        }else{
             return(
                <>
                <div style={{ display: 'flex', justifyContent: 'space-between'}}>
                  <div>
                      <h1>{this.props.title}</h1>
                  </div>
                  <div>
                    { this.props.selected != 'municipios' ? ( 
                      <button type="button" class="btn btn-primary" onClick={this.clicked}>
                          Cadastrar
                      </button>
                    ) : null
                    }
                  </div>
                </div>
                
                <DataTable
                    noHeader={true}
                    columns={columns}
                    data={this.props.data}
                    Clicked
                    onRowClicked={this.clicked}
                    pointerOnHover
                    pagination
                    paginationPerPage={5}
                />

                <Modal selected={this.props.selected} title={this.state.title} />
                
                </>
             )
        }

      }
}