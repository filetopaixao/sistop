const express = require('express')
const routes  = express.Router()

const LoginController = require('./controllers/LoginController')
const UserController = require('./controllers/UserController')
const NoticiasController = require('./controllers/NoticiasController')
const ProducaoController = require('./controllers/ProducaoController')
const ParceirosController = require('./controllers/ParceirosController')
const EquipeController = require('./controllers/EquipeController')
const ToponimiaController = require('./controllers/ToponimiaController')
const MunicipioController = require('./controllers/MunicipioController')

const multer = require('multer');
const uploadConfig = require('./config/upload');
//const ToponimiaController = require('./controllers/ToponimiaController')
//routes.post('/sessions', SessionController.create)

const upload = multer(uploadConfig);
var upload2 = multer({ dest: 'uploadd/' })

//user
routes.post('/login', LoginController.create)

routes.get('/usuarios', UserController.index)
routes.post('/usuarios', UserController.create)
routes.put('/usuarios/:id', UserController.update)
routes.delete('/usuarios/:id', UserController.delete)

//news
routes.get('/noticias', NoticiasController.index)
routes.post('/noticias', upload.single('image'), NoticiasController.create)
routes.put('/noticias/:id', upload.single('image'), NoticiasController.update)
routes.delete('/noticias/:id', NoticiasController.delete)
//routes.post('/noticias/uploads', upload.fields([{name: 'gallery'}]), NoticiasController.gallery);

//productions
routes.get('/producoes', ProducaoController.index)
routes.post('/producoes', upload.single('image'), ProducaoController.create)
routes.put('/producoes/:id', upload.single('image'), ProducaoController.update)
routes.delete('/producoes/:id', ProducaoController.delete)

//parceiros
routes.get('/parceiros', ParceirosController.index)
routes.post('/parceiros', upload.single('image'), ParceirosController.create)
routes.put('/parceiros/:id', upload.single('image'), ParceirosController.update)
routes.delete('/parceiros/:id', ParceirosController.delete)

//equipe
routes.get('/equipe', EquipeController.index)
routes.post('/equipe', upload.single('image'), EquipeController.create)
routes.put('/equipe/:id', upload.single('image'), EquipeController.update)
routes.delete('/equipe/:id', EquipeController.delete)

//toponimia
routes.get('/toponimias', ToponimiaController.index)
routes.post('/toponimias', upload.single('image'), ToponimiaController.create)
routes.put('/toponimias/:id', upload.single('image'), ToponimiaController.update)
routes.delete('/toponimias/:id', ToponimiaController.delete)

//municipios
routes.get('/municipios', MunicipioController.index)
routes.post('/municipios', upload.single('image'), ToponimiaController.create)
routes.put('/municipios/:id', upload.single('image'), ToponimiaController.update)
routes.delete('/municipios/:id', ToponimiaController.delete)

module.exports = routes