const connection = require('../database/connection')
const bcrypt = require('bcrypt');

module.exports = {
    async index(req, res) {
        const toponimias = await connection('toponimias')
        // pegar id passado
        // const id = request.headers.authorization
        return res.json({
            toponimias
        })
    },
    async create (req,res) {
        const image = 'http://localhost:3333/files/' + req.file.filename
        const {nome, long, lat, categoria, toponimia } = req.body
        const idUser = req.headers.authorization
        const data = await connection('toponimias').insert({
            nome,
            long,
            lat,
            categoria,
            image,
            toponimia,
            idUser
        })

        return res.json(data)
    },
    async update (req,res) {
        var id = parseInt(req.params.id);
        const {nome, long, lat, categoria, toponimia } = req.body
        const image = req.file ? 'http://localhost:3333/files/' + req.file.filename : req.body.image
        const data = await connection('toponimias').where('id' , id).update({
            nome,
            long,
            lat,
            categoria,
            image,
            toponimia,
            idUser
        })


        return res.json(data)
    },
    async delete (req,res) {
        var id = parseInt(req.params.id);

        await connection('toponimias').where('id' , id).del()

        return res.json({ "Success": "Deleted" })
    },
}