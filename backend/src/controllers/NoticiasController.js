const connection = require('../database/connection')

module.exports = {

    async index  (req, res) {
        // fazer paginação
        // const { page = 1 } = req.query //pega o valor do query page, se não existir ele vai ser 1
        // const [count] = await connection('users').count() //retorna quantos registros tem na tabela
        // const users = await connection('users').limit(5).offset().select('*') //busca os 5 primeiros do banco de dados
        // return res.header('X-Total-Count', count['count(*)]) // passa para o header da resposta a quantidade de registros que tem. 

        const noticias = await connection('noticias')
        // pegar id passado
        // const id = request.headers.authorization
        return res.json({
            noticias
        })
    },

    
    async create (req,res) {
        const image = 'http://localhost:3333/files/' + req.file.filename
        const {title, subtitle, author, news_date, content_text } = req.body
        const idUser = req.headers.authorization
        const published_date = new Date(Date.now())
        //console.log(req.file)
        const data = await connection('noticias').insert({
            title,
            subtitle,
            author,
            news_date,
            published_date,
            image,
            content_text,
            idUser
        })

        return res.json(data)
    },
    
    async update (req,res) {
        var id = parseInt(req.params.id);
        const image = req.file ? 'http://localhost:3333/files/' + req.file.filename : req.body.image
        console.log(req.body)
        const {title, subtitle, author, news_date, content_text } = req.body
        const published_date = new Date(Date.now())
        //console.log(req.file)
        const data = await connection('noticias').where('id' , id).update({
            title,
            subtitle,
            author,
            news_date,
            published_date,
            image,
            content_text
        })

        return res.json(data)
    },
    async delete (req,res) {
        var id = parseInt(req.params.id);

        await connection('noticias').where('id' , id).del()

        return res.json({ "Success": "Deleted" })
    },

}