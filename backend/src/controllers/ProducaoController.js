const connection = require('../database/connection')

module.exports = {

    async index  (req, res) {

        const producoes = await connection('producoes')
        // pegar id passado
        // const id = request.headers.authorization
        return res.json({
            producoes
        })
    },

    
    async create (req,res) {
        const {title, category, year, link, description} = req.body
        const idUser = req.headers.authorization
        const published_date = new Date(Date.now())
        //console.log(req.file)
        const data = await connection('producoes').insert({
            title,
            category,
            year,
            link,
            published_date,
            description,
            idUser
        })

        return res.json(data)
    },
    
    async update (req,res) {
        var id = parseInt(req.params.id);
        const {title, category, year, link, description } = req.body
        //console.log(req.file)
        const data = await connection('producoes').where('id' , id).update({
            title,
            category,
            year,
            link,
            description
        })

        return res.json(data)
    },
    async delete (req,res) {
        var id = parseInt(req.params.id);

        await connection('producoes').where('id' , id).del()

        return res.json({ "Success": "Deleted" })
    },

}