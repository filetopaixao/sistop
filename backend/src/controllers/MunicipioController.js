const connection = require('../database/connection')
const bcrypt = require('bcrypt');

module.exports = {
    async index(req, res) {
        const municipios = await connection('city')
        // pegar id passado
        // const id = request.headers.authorization
        return res.json({
            municipios
        })
    },
    async create (req,res) {
        const image = 'http://localhost:3333/files/' + req.file.filename
        const {nome, toponimia } = req.body
        const idUser = req.headers.authorization
        const data = await connection('municipios').insert({
            nome,
            image,
            toponimia,
            idUser
        })

        return res.json(data)
    },
    async update (req,res) {
        var id = parseInt(req.params.id);
        const { nome, toponimia } = req.body
        const image = req.file ? 'http://localhost:3333/files/' + req.file.filename : req.body.image
        const data = await connection('municipios').where('id' , id).update({
            nome,
            image,
            toponimia
        })


        return res.json(data)
    },
    async delete (req,res) {
        var id = parseInt(req.params.id);

        await connection('municipios').where('id' , id).del()

        return res.json({ "Success": "Deleted" })
    },
}