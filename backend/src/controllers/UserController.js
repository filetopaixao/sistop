const connection = require('../database/connection')
const bcrypt = require('bcrypt');

module.exports = {
    async index(req, res) {
        const usuarios = await connection('usuarios')
        // pegar id passado
        // const id = request.headers.authorization
        return res.json({
            usuarios
        })
    },
    async create(req, res) {
        const { name, username, password, role } = req.body
        const user = await connection('usuarios').where('username', username).select('*').first()
        const hash = bcrypt.hashSync(password, 10);
        if(!user){
            const status = 'ativo'
            const created_date = new Date(Date.now())
            //console.log(req.file)
            const data = await connection('usuarios').insert({
                name,
                username,
                password: hash,
                role,
                status,
                created_date
            })

            return res.json(data)
        }else{
            return res.status(400).json({ error: "User already exist." })
        }
    },
    async update (req,res) {
        var id = parseInt(req.params.id);
        const { name, username, password, role } = req.body
        const hash = bcrypt.hashSync(password, 10)
        const data = await connection('usuarios').where('id' , id).update({
            name,
            username,
            password: hash,
            role,
        })

        return res.json(data)
    },
    async delete (req,res) {
        var id = parseInt(req.params.id);

        await connection('usuarios').where('id' , id).del()

        return res.json({ "Success": "Deleted" })
    },
}