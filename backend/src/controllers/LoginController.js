const connection = require('../database/connection')
const bcrypt = require('bcrypt');

module.exports = {
    async create(req, res) {
        const { username, password } = req.body
        //const hash = bcrypt.hashSync(password, 10);
        //console.log(hash)
        const user = await connection('usuarios').where('username', username).select('*').first()

        if(!user){
            return res.status(400).json({ error: "No USER found with this USERNAME" })
        }else{
            
            //const pass = await connection('usuarios').where('password', password).select('*').first()
            if(!bcrypt.compareSync(password, user.password)){
                return res.status(400).json({ error: "No USER found with this PASSWORD" })
            }else{
                return res.json({id: user.id})
            }
        }
    }
}