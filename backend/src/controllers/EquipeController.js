const connection = require('../database/connection')
const bcrypt = require('bcrypt');

module.exports = {
    async index(req, res) {
        const equipe = await connection('equipe')
        // pegar id passado
        // const id = request.headers.authorization
        return res.json({
            equipe
        })
    },
    async create(req, res) {
        const { nome, formacao, atuacao, cargo, lattes, email, telefone, facebook, instagram, linkedin, twitter, skype } = req.body
        const image = req.file ? 'http://localhost:3333/files/' + req.file.filename : req.body.image
        const idUser = req.headers.authorization
        const published_date = new Date(Date.now())
        //console.log(req.file)
        const data = await connection('equipe').insert({
            nome,
            formacao,
            atuacao,
            cargo,
            lattes,
            email,
            telefone,
            facebook,
            instagram,
            linkedin,
            twitter,
            skype,
            published_date,
            idUser,
            foto: image
        })

        return res.json(data)
    },
    async update (req,res) {
        var id = parseInt(req.params.id);
        const { nome, formacao, atuacao, cargo, lattes, email, telefone, facebook, instagram, linkedin, twitter, skype } = req.body
        const image = req.file ? 'http://localhost:3333/files/' + req.file.filename : req.body.image
        const data = await connection('equipe').where('id' , id).update({
            nome,
            formacao,
            atuacao,
            cargo,
            lattes,
            email,
            telefone,
            facebook,
            instagram,
            linkedin,
            twitter,
            skype, foto: image
        })


        return res.json(data)
    },
    async delete (req,res) {
        var id = parseInt(req.params.id);

        await connection('equipe').where('id' , id).del()

        return res.json({ "Success": "Deleted" })
    },
}