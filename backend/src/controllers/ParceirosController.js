const connection = require('../database/connection')

module.exports = {

    async index  (req, res) {

        const parceiros = await connection('parceiros')
        return res.json({
            parceiros
        })
    },

    
    async create (req,res) {
        const image = 'http://localhost:3333/files/' + req.file.filename
        const {name, category, link } = req.body
        const idUser = req.headers.authorization
        const published_date = new Date(Date.now())
        const data = await connection('parceiros').insert({
            name,
            category,
            link,
            published_date,
            image,
            idUser
        })

        return res.json(data)
    },
    
    async update (req,res) {
        var id = parseInt(req.params.id);
        const image = req.file ? 'http://localhost:3333/files/' + req.file.filename : req.body.image
        const {name, category, link } = req.body
        const data = await connection('parceiros').where('id' , id).update({
            name,
            category,
            link,
            image
        })

        return res.json(data)
    },

    async delete (req,res) {
        var id = parseInt(req.params.id);

        await connection('parceiros').where('id' , id).del()

        return res.json({ "Success": "Deleted" })
    },

}