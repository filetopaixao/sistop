// Update with your config settings.
// npx knex init

module.exports = {

  /*
  development: {
    client: 'sqlite3',
    connection: {
      filename: './src/database/db.sqlite'
    },
    migrations: {
      directory: './src/database/migrations'
    },
    useNullAsDefault: true
  },
 

  development: {
    client: 'mysql',
    connection: {
      host : '162.241.252.194',
      user : 'toponimi_root',
      password : 'topo2016@KA...@',
      database : 'toponimi_novo_mtt'
    },
    migrations: {
      directory: './src/database/migrations'
    },
    useNullAsDefault: true
  }, 
  

  */
  development: {
    client: 'pg',
    version: '7.2',
    connection: {
      host : '127.0.0.1',
      user : 'postgres',
      password : '123456',
      database : 'toponimia_portal'
    },
    migrations: {
      directory: './src/database/migrations'
    },
    useNullAsDefault: true
  }, 
  staging: {
    client: 'postgresql',
    connection: {
      database: 'my_db',
      user:     'username',
      password: 'password'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },

  production: {
    client: 'postgresql',
    connection: {
      database: 'my_db',
      user:     'username',
      password: 'password'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  }

};
